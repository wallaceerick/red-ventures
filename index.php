<!doctype html>
<html lang="pt-BR">

	<head>
		
		<meta charset="UTF-8">

		<title>Teste: Red Ventures</title>

		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="apple-touch-icon" sizes="144x144" href="assets/images/icon/144.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="assets/images/icon/114.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="assets/images/icon/72.png" />
		<link rel="apple-touch-icon" sizes="577x57" href="assets/images/icon/57.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/icon/144.png" />
		<link rel="shortcut icon" type="image/x-icon" href="assets/images/icon/favicon.ico" />

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" />
		<link rel="stylesheet" type="text/css" href="assets/css/application.css" media="all" />

		<!--[if lt IE 9]>
			<script src="assets/js/html5.js"></script>
		<![endif]-->

	</head>

	<body class="js-loader">

		<?php include('includes/header.php'); ?>

		<section class="hero">
			<span class="wrapper">
				<span class="hero__content">
					<h2 class="hero__title">Collection 2016</h2>
					<p class="hero__text">Some cool text here about the new collection of <br />design furniture 2016</p>
					<a href="#" class="button primary-button hero__button">Buy Now</a>
				</span>
				<img class="hero__banner" src="assets/images/hero.jpg" alt="Collection 2016" />
			</span>
		</section> <!-- /hero -->

		<section class="filter">
			<span class="wrapper filter__wrapper">
				<form class="filter__form js-validate-contact" method="get" action="">
					<fieldset class="filter__set">
						<legend class="filter__title">Type:</legend>

						<label for="armchairs" class="filter__label">
							<i class="filter__checkbox"></i>
							<span>Armchairs</span>
							<input type="checkbox" id="armchairs" name="type" class="js-checkbox" />
						</label>

						<label for="chairs" class="filter__label">
							<i class="filter__checkbox"></i>
							<span>Chairs</span>
							<input type="checkbox" id="chairs" name="type" class="js-checkbox" />
						</label>
					</fieldset>

					<fieldset class="filter__set">
						<legend class="filter__title">Material:</legend>

						<label for="wood" class="filter__label">
							<i class="filter__checkbox"></i>
							<span>Wood</span>
							<input type="checkbox" id="wood" name="material" class="js-checkbox" />
						</label>

						<label for="foam" class="filter__label">
							<i class="filter__checkbox"></i>
							<span>Foam</span>
							<input type="checkbox" id="foam" name="material" class="js-checkbox" />
						</label>

					</fieldset>
				</form>
				<a href="#" class="filter__button button secundary-button">See All Products</a>
			</span>
		</section>

		<section class="products">
			<span class="wrapper">
				<h3 class="products__title">New Armchairs</h3>
				<div class="products__slider owl-carousel js-carousel">

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/1.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/2.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/3.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/4.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/5.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/6.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/7.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/8.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->
				</div> <!-- /products__slider -->

				<h3 class="products__title">Best of 2016</h3>
				<div class="products__slider owl-carousel js-carousel">

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/1.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/2.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/3.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/4.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/5.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/6.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/7.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->

					<div class="item products__item">
						<h4 class="products__category">Chair / Wood</h4>
						<img class="products__photo" src="assets/images/products/8.jpg" alt="Wood Design Chair" />
						<div class="products__info">
							<h5 class="products__info--name">Wood Design Chair</h5>
							<small class="products__info--price">$ 150,00</small>
							<a href="#" class="button secundary-button products__button">
								<i class="hide-text icon-cart-secundary products__button--icon"></i>
								<span>Buy Now</span>
							</a>
						</div>
					</div> <!-- /item -->
				</div> <!-- /products__slider -->
			</span>
		</section>

		<section class="about">
			<span class="wrapper">
				<h2 class="about__title">About Us</h2>
				<p class="about__text">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
			</span>
		</section> <!-- /about -->   

		<?php include('includes/footer.php'); ?>

	</body>

</html>