<header id="header" class="js-header">

    <h1 class="header__logo hide-text icon-logo-primary">
        <a href="#">Logo Name</a>
    </h1>

    <a href="#" class="header__pull-menu js-pull">Menu</a>
	<nav class="header__menu js-menu" role="navigation">
        <ul role="menubar">
            <li role="menuitem"><a href="#">Home</a></li>
            <li role="menuitem"><a href="#">Products</a></li>
            <li role="menuitem"><a href="#">Contact</a></li>
            <li role="menuitem" class="header--menu__last-item">
                <a href="#" class="hide-text icon-cart-primary js-cart-icon">My Cart</a>
            </li>
        </ul>
    </nav>

</header> 