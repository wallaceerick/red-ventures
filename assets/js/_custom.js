// |------------------------------------------------------
// |------------------------------------------------------
// | Custom JS
// | http://wallaceerick.com.br/
// | Copyright (c) 2016 Wallace Erick
// |------------------------------------------------------
// |------------------------------------------------------

var App = {

    init: function(){

        this.loader();
        this.header();
        this.menu();
        this.carousel();
        this.checkbox();

    },

    loader: function(){
        var loader = $('.js-loader');

        if(loader[0]){
            loader.jpreLoader({
                showPercentage: true,
                autoClose:      true,
                splashFunction: function(){
                    //console.log('Loading...');
                } 
            }, function(){
                //console.log('Loaded!');
            });
        }
    },

    header: function(){
        $(window).scroll(function(){
            var scrollTop      = $(this).scrollTop(),
                headerElement  = $('.js-header'),
                headerSize     = headerElement.height(),
                cartIcon       = $('.js-cart-icon');

            if(scrollTop >= headerSize){
                headerElement.addClass('scrolled');
                cartIcon.addClass('icon-cart-secundary');
            } else {
                headerElement.removeClass('scrolled');
                cartIcon.removeClass('icon-cart-secundary');
            }

        });
    },

    menu: function(){
        var menuButton  = $('.js-pull'),
            menuContent = $('.js-menu');

        menuButton.click(function(){
            menuContent.toggleClass('opened');
        });

    },

    carousel: function(){
        var carousel = $('.js-carousel');

        if(carousel[0]){
            carousel.owlCarousel({
                loop:          false,
                margin:        12,
                nav:           false,
                pagination:    true,
                scrollPerPage: true,
                responsive: {
                    320: {
                        items: 1,
                    },
                    435: {
                        items: 1,
                    },
                    550: {
                        items: 2,
                    },
                    768: {
                        items: 2,
                    },
                    960: {
                        items: 3,
                    },
                    1024: {
                        items: 4,
                    }
                }
           });
        }

    },

    checkbox: function(){
        var checkbox = $('.js-checkbox');

        if(checkbox[0]){
            checkbox.change(function(){
                if ($(this).is(':checked')){
                    $(this).parent().addClass('selected');
                } else {
                    $(this).parent().removeClass('selected');
                }
            });
        }

    }

};

App.init();
